import en from 'vuetify/es5/locale/en'

export default {
  // server: {
  //   port: 8000 // default: 3000
  // },
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: '%s | Art Rate',
    title: 'Art Rate',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css2?family=Ubuntu:wght@100;200;300;400;500;600;700;800;900',
      },
    ],
  },

  // https://fonts.googleapis.com/css2?family=Ubuntu:wght@300;400&display=swap

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['@/assets/scss/index.scss'],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: '/',
  },

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    meta: {
      name: 'Art Rate',
    },
    manifest: {
      name: 'Art Rate',
      lang: 'en',
    },
  },

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ['~/assets/scss/variables.scss'],
    treeShake: true,
    rtl: false,
    lang: {
      current: 'en',
      locales: { en },
    },
    font: {
      family: 'Ubuntu',
    },
    theme: {
      dark: false,
      themes: {
        light: {
          primary: '#cd6109',
          accent: '#7a5171',
          secondary: '#2f4858',
          info: '#057b7b',
          warning: '#cd8c09',
          error: '#c45259',
          success: '#63baab',
        },
      },
      options: { customProperties: true },
    },
    breakpoint: {
      mobileBreakpoint: 'xs', // This is equivalent to a value of 600px
    },
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},
}
