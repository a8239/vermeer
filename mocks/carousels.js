export const home = [
  {
    name: null,
    title: 'Collect art by the world’s leading artists',
    subtitle: 'Register for updates on available works, market news, and more.',
    image_url:
      'https://d7hftxdivxxvm.cloudfront.net/?resize_to=fill&src=http%3A%2F%2Ffiles.artsy.net%2Fhomepage%2Fsignup-banner.png&width=910&height=607&quality=75',
    button: {
      name: 'Sign Up',
      to: { name: '' },
    },
  },
  {
    name: 'Art Rate Auctions',
    title: 'Bid on Art by Today’s Leading Artists',
    subtitle: 'Find works by emerging and established artists at auction',
    image_url:
      'https://d7hftxdivxxvm.cloudfront.net/?resize_to=fill&src=https%3A%2F%2Fd32dm0rphc51dk.cloudfront.net%2FIopjCK3x5TuGzjFqp-K3rQ%2Funtouched-jpg.jpg&width=910&height=607&quality=75',
    button: {
      name: 'Browse Works',
      to: { name: '' },
    },
  },
]
