export const auctions = [
  {
    id: 1,
    slug: 'balloon-swan-2017',
    name: '76 * 96',
    artist: 'Nasser Oveysi',
    price: '$ 10000',
    image_url: require('@/assets/images/Auction/01.png')
  },
  {
    id: 2,
    slug: 'Flowarh-2021',
    name: '60 * 180',
    artist: 'Manouchehr Niazi',
    price: '$ 7000',
    image_url: require('@/assets/images/Auction/02.png')
  },
  {
    id: 3,
    slug: 'Light-Trap-2018',
    name: '140 * 100',
    artist: 'Mahmoud Zanganeh',
    price: '$ 5000',
    image_url: require('@/assets/images/Auction/03.png')
  },
  {
    id: 4,
    slug: 'untitled-2017',
    name: '70 * 50',
    artist: 'Jalal Shabahangi',
    price: '$ 4000',
    image_url: require('@/assets/images/Auction/04.png')
  },

  {
    id: 5,
    slug: 'Little-Big-Head-2020',
    name: '70 * 90',
    artist: 'Hossein Mahjoubi',
    price: '$ 9000',
    image_url: require('@/assets/images/Auction/05.png')
  },
  {
    id: 6,
    slug: 'love-letters-2021',
    name: '70 * 90',
    artist: 'Ali Golestaneh',
    price: '$ 5000',
    image_url: require('@/assets/images/Auction/06.png')
  },
  // big
  {
    id: 7,
    slug: 'What-If-Women-Ruled-the-World-2020',
    name: '50 * 50',
    artist: 'Manouchehr Motabar',
    price: '$ 4000',
    image_url: require('@/assets/images/Auction/07.png')
  },
  {
    id: 8,
    slug: 'What-If-Women-Ruled-2020',
    name: '60 * 90',
    artist: 'Dariush Zahedi',
    price: '$ 3000',
    image_url: require('@/assets/images/Auction/08.png')
  }, {
    id: 9,
    slug: 'What-If-Women-2020',
    name: '80 * 80',
    artist: 'Dariush Zahedi',
    price: '$ 4000',
    image_url: require('@/assets/images/Auction/09.png')
  }
]
export const shows = [
  {
    id: 1,
    slug: 'AB-Projects-The-David-Shrigley-Edit',
    title: 'AB Projects: The David Shrigley Edit',
    subtitle: 'AB Projects',
    date: 'Jan 14–Feb 1',
    image_url:
      'https://d7hftxdivxxvm.cloudfront.net/?resize_to=fill&width=325&height=230&quality=80&src=https%3A%2F%2Fd32dm0rphc51dk.cloudfront.net%2FCDdQw0hvHz5L4alfFBXQSw%2Flarge.jpg',
  },
  {
    id: 2,
    slug: 'of-a-clearing',
    title: '….)( of, a clearing',
    subtitle: 'White Cube',
    date: 'Jan 21–Mar 12',
    image_url:
      'https://d7hftxdivxxvm.cloudfront.net/?resize_to=fill&width=325&height=230&quality=80&src=https%3A%2F%2Fd32dm0rphc51dk.cloudfront.net%2FJNmRhMWgwq8kJqo67Drrxg%2Flarge.jpg',
  },
  {
    id: 3,
    slug: 'Group-show-curated-by-106-Green---Still-Going',
    title: 'Group show curated by 106 Green - Still Going',
    subtitle: 'Taymour Grahne Projects',
    date: 'Jan 22–Feb 19',
    image_url:
      'https://d7hftxdivxxvm.cloudfront.net/?resize_to=fill&width=325&height=230&quality=80&src=https%3A%2F%2Fd32dm0rphc51dk.cloudfront.net%2FdVhzUjgGkI1srlNFGImiPA%2Flarge.jpg',
  },
  {
    id: 4,
    slug: 'nieta',
    title: 'nieta',
    subtitle: "Sargent's Daughters",
    date: 'Jan 7–Feb 5',
    image_url:
      'https://d7hftxdivxxvm.cloudfront.net/?resize_to=fill&width=325&height=230&quality=80&src=https%3A%2F%2Fd32dm0rphc51dk.cloudfront.net%2F0YXlA-YYV6rybKHGJBz0EA%2Flarge.jpg',
  },
  {
    id: 5,
    slug: 'nieta',
    title: `Edie Nadelhaft "We Ain't Goin' Nowhere"`,
    subtitle: 'Kasper Contemporary',
    date: 'Jan 13–Feb 20',
    image_url:
      'https://d7hftxdivxxvm.cloudfront.net/?resize_to=fill&width=325&height=230&quality=80&src=https%3A%2F%2Fd32dm0rphc51dk.cloudfront.net%2FkmRa8DipxSliF-cEDKHmDA%2Flarge.jpg',
  },
  {
    id: 6,
    slug: 'Necessity',
    title: 'Necessity',
    subtitle: 'Galleri Magnus Karlsson',
    date: 'Jan 22–Mar 5',
    image_url:
      'https://d7hftxdivxxvm.cloudfront.net/?resize_to=fill&width=325&height=230&quality=80&src=https%3A%2F%2Fd32dm0rphc51dk.cloudfront.net%2FZc_VKz0rG4ussWrhC7ng7A%2Flarge.jpg',
  },
]
export const featuredFairs = [
  {
    id: 1,
    slug: 'balloon-swan-2017',
    title: 'Auction of Dariush Zahedi Art works 2022',
    subtitle: 'Featured Fair',
    image_url: require('@/assets/images/Featured/01.gif'),
  },
  {
    id: 2,
    slug: 'balloon-swan-2017',
    title: 'Group exhibition of artists, Romina Art Gallery 2022',
    subtitle: 'Featured Fair',
    image_url: require('@/assets/images/Featured/02.gif'),
  },
]
export const currentFairs = [
  {
    id: 1,
    slug: 'balloon-swan-2017',
    title: 'LA Art Show 2022',
    date: 'January 17 – February 5, 2022',
    image_url:
      'https://d7hftxdivxxvm.cloudfront.net/?resize_to=fill&width=600&height=450&quality=80&src=https%3A%2F%2Fd32dm0rphc51dk.cloudfront.net%2FnVheVI9ImmPEIDzlYGTqHg%2Fwide.jpg',
  },
  {
    id: 2,
    slug: 'balloon-swan-2017',
    title: 'S.E.A. Focus 2022',
    date: 'January 15 – 30, 2022',
    image_url:
      'https://d7hftxdivxxvm.cloudfront.net/?resize_to=fill&width=600&height=450&quality=80&src=https%3A%2F%2Fd32dm0rphc51dk.cloudfront.net%2FOQlHPtIHA2uscw0DYEiZNA%2Fwide.jpg',
  },
]
export const bigNews = {
  id: 1,
  slug: 'Why-Collectors-Buy-NFTs-and-How-to-Get-Started',
  title: 'The #death_face portrait collection of "Romina Zahedi" latest project will be published soon',
  subject: 'Art',
  writer: '#Art_rate',
  image_url: require('@/assets/images/market-news/01.gif')
}
export const news = [
  {
    id: 2,
    slug: 'What-is-Really-at-Stake-in-the-Debate-over-Whether-NFTs-Are-Art',
    title: 'What’s Really at Stake in the Debate over Whether NFTs Are Art',
    subject: 'ART',
    writer: 'Art Rate Editorial',
    image_url:
      'https://d7hftxdivxxvm.cloudfront.net/?resize_to=fill&width=325&height=240&quality=80&src=https%3A%2F%2Fartsy-media-uploads.s3.amazonaws.com%2FMiyCVQgwfkfD_bVJxKmPAg%252FMAG%2BTHUMB_MMarple_Medusa56_MedusaCollection.jpg',
  },
  {
    id: 7,
    slug:
      'Kota-Ezawa-Flat,-Stylized-Paintings-and-Animations-Shed-New-Light-on-Historic-Events',
    title:
      'Kota Ezawa’s Flat, Stylized Paintings and Animations Shed New Light on Historic Events',
    subject: 'ART',
    writer: 'Art Rate Editorial',
    image_url:
      'https://d7hftxdivxxvm.cloudfront.net/?resize_to=fill&width=325&height=240&quality=80&src=https%3A%2F%2Fartsy-media-uploads.s3.amazonaws.com%2F9Cpo7Hmy71iUvRKlkOt3fg%252FMAG.jpg',
  },
  {
    id: 3,
    slug:
      'In-Cho-Gi-Seok-Striking-Photographs,-Humans-and-Nature-Coexist-in-Uneasy-Ways',
    title:
      'In Cho Gi-Seok’s Striking Photographs, Humans and Nature Coexist in Uneasy Ways',
    subject: 'ART',
    writer: 'Art Rate Editorial',
    image_url:
      'https://d7hftxdivxxvm.cloudfront.net/?resize_to=fill&width=325&height=240&quality=80&src=https%3A%2F%2Fartsy-media-uploads.s3.amazonaws.com%2FdDq25aEAwMqoRdGa7tfF4g%252FFotografiska%2BNY_Cho%2BGi-Seok_18.jpg',
  },
  {
    id: 4,
    slug: 'Paula-Rego-Darkly-Ambiguous-Paintings-Reflect-on-Abuses-of-Power',
    title: 'Paula Rego’s Darkly Ambiguous Paintings Reflect on Abuses of Power',
    subject: 'ART',
    writer: 'Art Rate Editorial',
    image_url:
      'https://d7hftxdivxxvm.cloudfront.net/?resize_to=fill&width=325&height=240&quality=80&src=https%3A%2F%2Fartsy-media-uploads.s3.amazonaws.com%2FyfG1RhLcthM0As6znQDyrg%252FMAG2.jpg',
  },

]

export const optionalCarousels = [
  {
    key: 'serial',
    title: null,
    subtitle:
      'The first serial release of portraits of the death_face #collection',
    description: null,
    buttonText: null,
    wide: true,
    image_url: require('@/assets/images/slide0/slide0.gif'),
  },
  {
    key: 'phenomenon',
    title: null,
    subtitle: 'Each layer of the work of art describes a phenomenon.',
    description:
      'You will soon be able to attach millions of pieces of information to your work and describe your world.',
    buttonText: null,
    image_url: require('@/assets/images/slide01/01.gif'),
  },
  {
    key: 'works',
    title: null,
    subtitle: 'Collect works by talented Mesopotamian artists',
    description:
      'Register to update existing works, collaborate on the production of works and more.',
    buttonText: null,
    image_url: require('@/assets/images/slide02/02.gif'),
  },
  {
    key: 'join',
    title: null,
    subtitle: '#Art_Rate will join the edgware network in 2022',
    description:
      'Zein is our professional partner who has integrated NFT solution with WhatsApp.',
    buttonText: null,
    image_url: require('@/assets/images/slide03/03.gif'),
  },
]

export const galleries = [
  {
    id: 1,
    slug: 'browse_works',
    title: 'Out of Africa Gallery',
    subtitle: 'Sitges - Barcelona',
    image_url:
      'https://d7hftxdivxxvm.cloudfront.net/?resize_to=fill&src=https%3A%2F%2Fd32dm0rphc51dk.cloudfront.net%2FIopjCK3x5TuGzjFqp-K3rQ%2Funtouched-jpg.jpg&width=910&height=607&quality=75',
  },
  {
    id: 2,
    slug: 'browse_works',
    title: 'Artch Gallery',
    subtitle: 'Bozeman',
    image_url:
      'https://d7hftxdivxxvm.cloudfront.net/?resize_to=fill&width=445&height=334&quality=80&src=https%3A%2F%2Fd32dm0rphc51dk.cloudfront.net%2FwS87h6ZlWvgoTvuef1rM-A%2Fwide.jpg',
  },
  {
    id: 3,
    slug: 'browse_works',
    title: 'Adelson Galleries',
    subtitle: 'New York, Palm Beach',
    image_url:
      'https://d7hftxdivxxvm.cloudfront.net/?resize_to=fill&width=445&height=334&quality=80&src=https%3A%2F%2Fd32dm0rphc51dk.cloudfront.net%2F75KrvLEwwTNGbRerxbx9jg%2Fwide.jpg',
  },
  {
    id: 4,
    slug: 'browse_works',
    title: 'Seasons LA',
    subtitle: 'Los Angeles',
    image_url:
      'https://d7hftxdivxxvm.cloudfront.net/?resize_to=fill&width=445&height=334&quality=80&src=https%3A%2F%2Fd32dm0rphc51dk.cloudfront.net%2Frf1B1IVt0tNabryE0iM5qA%2Fwide.jpg',
  },
  {
    id: 5,
    slug: 'browse_works',
    title: 'The Contemporary Art Modern Project',
    subtitle: 'Miami, Westport',
    image_url:
      'https://d7hftxdivxxvm.cloudfront.net/?resize_to=fill&width=445&height=334&quality=80&src=https%3A%2F%2Fd32dm0rphc51dk.cloudfront.net%2F_63_Gw5x5-FILVgi7ceHGQ%2Fwide.jpg',
  },
  {
    id: 6,
    slug: 'browse_works',
    title: 'Gazelli Art House',
    subtitle: 'London, Baku',
    image_url:
      'https://d7hftxdivxxvm.cloudfront.net/?resize_to=fill&width=445&height=334&quality=80&src=https%3A%2F%2Fd32dm0rphc51dk.cloudfront.net%2FwD1EDqW9zAjm5Hkld7DuWw%2Fwide.jpg',
  },
  {
    id: 7,
    slug: 'browse_works',
    title: 'Romero Paprocki',
    subtitle: 'Paris',
    image_url:
      'https://d7hftxdivxxvm.cloudfront.net/?resize_to=fill&width=445&height=334&quality=80&src=https%3A%2F%2Fd32dm0rphc51dk.cloudfront.net%2FzkCDZleHqBGnW6THE-bZ6g%2Fwide.jpg',
  },
  {
    id: 8,
    slug: 'browse_works',
    title: 'Spazio Nobile',
    subtitle: 'Brussels, Bruxelles',
    image_url:
      'https://d7hftxdivxxvm.cloudfront.net/?resize_to=fill&width=445&height=334&quality=80&src=https%3A%2F%2Fd32dm0rphc51dk.cloudfront.net%2F_RrsZuBvBASa6cdN4JkdaQ%2Fwide.jpg',
  },
  {
    id: 9,
    slug: 'browse_works',
    title: 'Galerie aKonzept',
    subtitle: 'Berlin',
    image_url:
      'https://d7hftxdivxxvm.cloudfront.net/?resize_to=fill&width=445&height=334&quality=80&src=https%3A%2F%2Fd32dm0rphc51dk.cloudfront.net%2FP8rv7XTq0vTFHo7LatW6ZA%2Fwide.jpg',
  },
  {
    id: 10,
    slug: 'browse_works',
    title: 'Zawyeh Gallery',
    subtitle: 'Ramallah, Dubai',
    image_url:
      'https://d7hftxdivxxvm.cloudfront.net/?resize_to=fill&width=445&height=334&quality=80&src=https%3A%2F%2Fd32dm0rphc51dk.cloudfront.net%2FslvphW-oepN-EZVPR4dHVw%2Fwide.jpg',
  },
]

export const artists = [
  {
    id: 1,
    slug: 'browse_works',
    title: 'Robert Zhao Renhui',
    subtitle: 'Singaporean, b. 1983',
    image_url:
      'https://d7hftxdivxxvm.cloudfront.net/?resize_to=fill&width=325&height=230&quality=80&src=https%3A%2F%2Fd32dm0rphc51dk.cloudfront.net%2FApW-11-NoNGOq9y2Sjsj6Q%2Flarge.jpg',
  },
  {
    id: 1,
    slug: 'browse_works',
    title: 'Mike Piggott',
    subtitle: 'American',
    image_url:
      'https://d7hftxdivxxvm.cloudfront.net/?resize_to=fill&width=325&height=230&quality=80&src=https%3A%2F%2Fd32dm0rphc51dk.cloudfront.net%2FWhLDxFLuUeJVlcegUyL9Wg%2Flarge.jpg',
  },
  {
    id: 1,
    slug: 'browse_works',
    title: 'Li Huasheng 李华生',
    subtitle: 'Chinese, 1944–2018',
    image_url:
      'https://d7hftxdivxxvm.cloudfront.net/?resize_to=fill&width=325&height=230&quality=80&src=https%3A%2F%2Fd32dm0rphc51dk.cloudfront.net%2FGz8J1Qyufiktk79mI9qIsw%2Flarge.jpg',
  },
  {
    id: 1,
    slug: 'browse_works',
    title: 'Dramane Diarra',
    subtitle: 'Mali, b. 1994',
    image_url:
      'https://d7hftxdivxxvm.cloudfront.net/?resize_to=fill&width=325&height=230&quality=80&src=https%3A%2F%2Fd32dm0rphc51dk.cloudfront.net%2FPSOtts4Yxv0hPW8r6V0M1w%2Flarge.jpg',
  },
  {
    id: 1,
    slug: 'browse_works',
    title: 'Abel Quezada',
    subtitle: 'Mexican, 1920–1991',
    image_url:
      'https://d7hftxdivxxvm.cloudfront.net/?resize_to=fill&width=325&height=230&quality=80&src=https%3A%2F%2Fd32dm0rphc51dk.cloudfront.net%2F8pyLY5yo99HqMyCNU242yw%2Flarge.jpg',
  },
  {
    id: 1,
    slug: 'browse_works',
    title: 'Craig Dorety',
    subtitle: 'American, b. 1973',
    image_url:
      'https://d7hftxdivxxvm.cloudfront.net/?resize_to=fill&width=325&height=230&quality=80&src=https%3A%2F%2Fd32dm0rphc51dk.cloudfront.net%2FazQWjap88RslHh_9rh0iAw%2Flarge.jpg',
  },
  {
    id: 1,
    slug: 'browse_works',
    title: 'Arthur Jafa',
    subtitle: 'American, b. 1960',
    image_url:
      'https://d7hftxdivxxvm.cloudfront.net/?resize_to=fill&width=325&height=230&quality=80&src=https%3A%2F%2Fd32dm0rphc51dk.cloudfront.net%2FbNkgo54ksYARCgziZZ7qSA%2Flarge.jpg',
  },
  {
    id: 1,
    slug: 'browse_works',
    title: 'René Tavares',
    subtitle: 'Sao Tomean, b. 1983',
    image_url:
      'https://d7hftxdivxxvm.cloudfront.net/?resize_to=fill&width=325&height=230&quality=80&src=https%3A%2F%2Fd32dm0rphc51dk.cloudfront.net%2F5H_cTjJzDalbkGw0E4wfLw%2Flarge.jpg',
  },
  {
    id: 1,
    slug: 'browse_works',
    title: 'Aldo Mondino',
    subtitle: 'Italian, 1938–2005',
    image_url:
      'https://d7hftxdivxxvm.cloudfront.net/?resize_to=fill&width=325&height=230&quality=80&src=https%3A%2F%2Fd32dm0rphc51dk.cloudfront.net%2FFdVinUp8PTqzNKHMkqeMZA%2Flarge.jpg',
  },
  {
    id: 1,
    slug: 'browse_works',
    title: 'Suekí',
    subtitle: 'Angolan, b. 1981',
    image_url:
      'https://d7hftxdivxxvm.cloudfront.net/?resize_to=fill&width=325&height=230&quality=80&src=https%3A%2F%2Fd32dm0rphc51dk.cloudfront.net%2FQCgRnH9Rb9cb6oJNxfqtTw%2Flarge.jpg',
  },
  {
    id: 1,
    slug: 'browse_works',
    title: 'Roscoe Hall',
    subtitle: 'American, b. 1978',
    image_url:
      'https://d7hftxdivxxvm.cloudfront.net/?resize_to=fill&width=325&height=230&quality=80&src=https%3A%2F%2Fd32dm0rphc51dk.cloudfront.net%2FI7fP9tGIruyKpjXukhIiVA%2Flarge.jpg',
  },
  {
    id: 1,
    slug: 'browse_works',
    title: 'Jean Crotti',
    subtitle: 'French, 1878–1958',
    image_url:
      'https://d7hftxdivxxvm.cloudfront.net/?resize_to=fill&width=325&height=230&quality=80&src=https%3A%2F%2Fd32dm0rphc51dk.cloudfront.net%2Fgz73WFdUDwhw1as7XubuxQ%2Flarge.jpg',
  },
  {
    id: 1,
    slug: 'browse_works',
    title: 'Walter Schels',
    subtitle: 'German, b. 1936',
    image_url:
      'https://d7hftxdivxxvm.cloudfront.net/?resize_to=fill&width=325&height=230&quality=80&src=https%3A%2F%2Fd32dm0rphc51dk.cloudfront.net%2FI7fP9tGIruyKpjXukhIiVA%2Flarge.jpg',
  },
  {
    id: 1,
    slug: 'browse_works',
    title: "Ana D' Castro",
    subtitle: 'Portuguese, b. 1984',
    image_url:
      'https://d7hftxdivxxvm.cloudfront.net/?resize_to=fill&width=325&height=230&quality=80&src=https%3A%2F%2Fd32dm0rphc51dk.cloudfront.net%2F4pzLkQlw48uxXSz53DYV-w%2Flarge.jpg',
  },
  {
    id: 1,
    slug: 'browse_works',
    title: 'Javier Ruíz Pérez',
    subtitle: 'Spanish, b. 1989',
    image_url:
      'https://d7hftxdivxxvm.cloudfront.net/?resize_to=fill&width=325&height=230&quality=80&src=https%3A%2F%2Fd32dm0rphc51dk.cloudfront.net%2Fp9xfUBskFSyOKfYRWsWPlQ%2Flarge.jpg',
  },
  {
    id: 1,
    slug: 'browse_works',
    title: 'Samuel Fosso',
    subtitle: 'Cameroonian, b. 1962',
    image_url:
      'https://d7hftxdivxxvm.cloudfront.net/?resize_to=fill&width=325&height=230&quality=80&src=https%3A%2F%2Fd32dm0rphc51dk.cloudfront.net%2FN0QewVgk9QWhCqj-JQUkPw%2Flarge.jpg',
  },
  {
    id: 1,
    slug: 'browse_works',
    title: 'Tega Akpokona',
    subtitle: 'Nigerian, b. 1991',
    image_url:
      'https://d7hftxdivxxvm.cloudfront.net/?resize_to=fill&width=325&height=230&quality=80&src=https%3A%2F%2Fd32dm0rphc51dk.cloudfront.net%2FAzpdgGCKQqskiFePyOIVmQ%2Flarge.jpg',
  },
]
/*






























*/
