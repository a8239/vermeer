const prefix = 'path'
/**
 * REMOVE EXTRA METHODS WHEN COPY IT
 */
export default {
  /**
   * comment
   *
   * @param { commit } context
   * @param { config } data
   * @return Promise<object>
   */
  async GET_ALL_ACTION({ commit }, { config = {} }) {
    return await this.$axios.get(prefix, config)
  },

  /**
   * comment
   *
   * @param { commit } context
   * @param { resource, config } data
   * @return Promise<object>
   */
  async GET_BY_ID_ACTION({ commit }, { resource, config = {} }) {
    return await this.$axios.get(`${prefix}/${resource}`, config)
  },

  /**
   * comment
   *
   * @param { commit } context
   * @param { payload, config } data
   * @return Promise<object>
   */
  async CREATE_ACTION({ commit }, { payload, config = {} }) {
    return await this.$axios.post(prefix, payload, config)
  },

  /**
   * comment
   *
   * @param { commit } context
   * @param { resource, payload, config } data
   * @return Promise<object>
   */
  async UPDATE_ACTION({ commit }, { resource, payload, config = {} }) {
    return await this.$axios.put(`${prefix}/${resource}`, payload, config)
  },

  /**
   * comment
   *
   * @param { commit } context
   * @param { resource, config } data
   * @return Promise<object>
   */
  async DELETE_ACTION({ commit }, { resource, config = {} }) {
    return await this.$axios.delete(`${prefix}/${resource}`, config)
  },

  /**
   * comment
   *
   * @param { commit } context
   * @param { resource, config } data
   * @return Promise<object>
   */
  async ARCHIVE_ACTION({ commit }, { resource, config = {} }) {
    return await this.$axios.patch(prefix + `/${resource}/archive`, config)
  },

  /**
   * comment
   *
   * @param { commit } context
   * @param { resource, config } data
   * @return Promise<object>
   */
  async RESTORE_ARCHIVE_ACTION({ commit }, { resource, config = {} }) {
    return await this.$axios.patch(
      prefix + `/${resource}/restore-archive`,
      config
    )
  },
}
